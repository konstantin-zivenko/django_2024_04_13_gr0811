# django_2024_04_13_gr0811

---

[video 1](https://youtu.be/rh2dcy9O5p8) - Django 2024-04-13, intro

[video 2](https://youtu.be/1Q5U8JU2J1A) - Django ORM, models, fields, objects, save(), delete(), 2024-04-17

[video 3](https://youtu.be/0VkbxkHN2AY) - Django ORM, relations, 2024-04-20

[video 4](https://youtu.be/cXAKOpyo6M4) - Django ORM, queries, 2024-04-24

[video 5](https://youtu.be/84iCekS6cgU) - Django views and templates, part 1, 2024-05-01

