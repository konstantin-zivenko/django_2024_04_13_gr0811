import warnings

from django.db import transaction
from django.db.models import QuerySet

from .models import Author, Book, LiteraryGenre


def get_authors(
    id_: int = None, /, first_name: str = None, last_name: str = None
) -> Author | QuerySet[Author]:
    if id_ and first_name or last_name:
        warnings.warn(
            f"function {get_authors.__name__}: Both id_ and first_name/last_name were provided. id_ will be used."
        )
    queryset = Author.objects.all()
    if id_:
        queryset = queryset.filter(id=id_)
        return queryset.first()
    if first_name:
        queryset = queryset.filter(first_name__icontains=first_name)
    if last_name:
        queryset = queryset.filter(last_name__icontains=last_name)
    return queryset


def create_author(first_name: str, last_name: str) -> Author:
    return Author.objects.create(first_name=first_name, last_name=last_name)


def get_books(
    id_: int = None,
    title: str = None,
    description: str = None,
    price_from: float = None,
    price_to: float = None,
    type_: str = None,
    genre: LiteraryGenre = None,
    authors_ids: list[int] = None,
) -> Book | QuerySet[Book]:
    if id_:
        if title or description or price or type_ or genre or authors_ids:
            warnings.warn(
                f"function {get_books.__name__}:\n"
                f"Both id_ and title/description/price/type_/genre/authors_ids were provided. id_ will be used."
            )
        return Book.objects.get(id=id_)

    queryset = Book.objects.all()

    if title:  # field_name__lookuptype=value
        queryset = queryset.filter(title__icontains=title)
    if description:
        queryset = queryset.filter(description__icontains=description)
    if price_from:
        queryset = queryset.filter(price__gte=price_from)
    if price_to:
        queryset = queryset.filter(price__lte=price_to)
    if type_:
        queryset = queryset.filter(type=type_)
    if genre:
        queryset = queryset.filter(genre=genre)
    if authors_ids:
        queryset = queryset.filter(authors__id__in=authors_ids)
    return queryset


def create_book(
    title: str,
    description: str,
    price: float,
    type_: str,
    genre: LiteraryGenre,
    authors: list[Author],
) -> Book | None:
    # add transaction management
    with transaction.atomic():
        new_book = Book.objects.create(
            title=title, description=description, price=price, type=type_, genre=genre
        )
        new_book.authors.set(authors)
        return new_book
