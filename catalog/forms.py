from django import forms

from catalog.models import Author


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = "__all__"  # or ['first_name', 'last_name', 'date_of_birth', 'date_of_death', 'description']
