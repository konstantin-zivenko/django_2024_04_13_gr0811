import datetime

from django.db import models
from django.urls import reverse


class LiteraryGenre(models.Model):
    genre = models.CharField(
        max_length=16,
        help_text="Enter a literary genre (e.g. Science Fiction, French Poetry etc.)",
        unique=True,
    )

    def __str__(self):
        return f"{self.id} - {self.genre}"


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField("Died", null=True, blank=True)
    description = models.TextField(default="No description available")

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"


class Book(models.Model):
    ItemType = (
        ("BOOK", "Book"),
        ("MAGAZINE", "Magazine"),
        ("NEWSPAPER", "Newspaper"),
        ("JOURNAL", "Journal"),
        ("BOARDGAME", "Boardgame"),
        ("OTHER", "Other"),
    )
    title = models.CharField(max_length=200)
    description = models.TextField(default="No description available")
    price = models.DecimalField(max_digits=8, decimal_places=2)
    added_on = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=10, choices=ItemType, default="BOOK")
    genre = models.ForeignKey(
        LiteraryGenre, on_delete=models.CASCADE, related_name="books", null=True
    )
    authors = models.ManyToManyField(Author, related_name="books")

    def __str__(self):
        return (
            f"id: {self.id}, {self.title}, price={self.price}\n"
            f"{self.description}\n"
            f"added: {self.added_on}"
        )

    def get_absolute_url(self):
        return reverse("catalog:book-detail", args=[str(self.id)])
