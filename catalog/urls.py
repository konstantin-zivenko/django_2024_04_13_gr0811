from django.urls import path
from .views import index, LiteraryGenreListView, BookListView, AuthorListView, BookDetailView, test_session_view, AuthorDetailView, author_create

# https://docs.djangoproject.com/en/5.0/topics/http/urls/

urlpatterns = [
    path("", index, name="index"),
    path("literary-genres/", LiteraryGenreListView.as_view(), name="literary-genre-list"),
    path("books/", BookListView.as_view(), name="book-list"),
    path("books/<int:pk>/", BookDetailView.as_view(), name="book-detail"),
    path("authors/", AuthorListView.as_view(), name="author-list"),
    path("authors/<int:pk>/", AuthorDetailView.as_view(), name="author-detail"),
    path("authors/create/", author_create, name="author-create"),
    path("test-sessions/", test_session_view, name="test-session"),
]

app_name = "catalog"
