from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpRequest, Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from .forms import AuthorForm
from .models import Book, Author, LiteraryGenre


def test_session_view(request: HttpRequest) -> HttpResponse:
    request.session["book"] = "Test session book"
    return HttpResponse(
        "<h1>Test session view</h1>"
        f"<h4>Session data: {request.session['book']}</h4>"
    )


def index(request: HttpRequest) -> HttpResponse:
    num_book = Book.objects.count()
    num_author = Author.objects.count()
    num_genre = LiteraryGenre.objects.count()
    time_and_date = datetime.now()
    num_visits = request.session.get("num_visits", 0)
    request.session["num_visits"] = num_visits + 1

    return render(
        request,
        "catalog/index.html",
        context={
            "num_books": num_book,
            "num_authors": num_author,
            "num_genres": num_genre,
            "datetime": time_and_date,
            "num_visits": num_visits + 1,
        },
    )


# https://docs.djangoproject.com/en/5.0/topics/class-based-views/generic-display/
class LiteraryGenreListView(LoginRequiredMixin, generic.ListView):
    model = LiteraryGenre
    context_object_name = "literary_genre_list"  # показати як формуються імена за замовченням
    template_name = "catalog/literary_genre_list.html"  # показати як формуються шаблони за замовченням


class BookListView(LoginRequiredMixin, generic.ListView):
    model = Book
    queryset = Book.objects.select_related("genre")
    paginate_by = 3


class AuthorListView(LoginRequiredMixin, generic.ListView):
    model = Author


class BookDetailView(LoginRequiredMixin, generic.DetailView):
    model = Book


class AuthorDetailView(LoginRequiredMixin, generic.DetailView):
    model = Author


# def book_detail_view(request: HttpRequest, pk: int) -> HttpResponse:
#     try:
#         book = Book.objects.get(pk=pk)
#     except Book.DoesNotExist:
#         raise Http404("Book does not exist")
#     return render(request, "catalog/book_detail.html", context={"book": book})


def author_create(request: HttpRequest) -> HttpResponse:
    context = {}
    form = AuthorForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse("catalog:author-list"))
    context["form"] = form
    return render(request, "catalog/author_form.html", context)
